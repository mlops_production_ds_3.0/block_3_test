# start_and_format 

Docker образ принимает на вход файл формата `.py` и запускает его, используя прописанные библиотеки. Помимо отработки файла, Docker образ включает себя линтер ([flake8](https://pypi.org/project/flake8/)) и форматтеры ([ruff](https://github.com/astral-sh/ruff), [isort](https://pycqa.github.io/isort/)).

- [start_and_format](#start_and_format)
- [Описание](#описание)
- [Быстрый старт](#быстрый-старт)
- [Используя линтер и форматтеры](#используя-линтер-и-форматтеры) \
    - [flake8](#flake8) \
    - [ruff and isort](#ruff-and-isort)
- [Используемые зависимости](#используемые-зависимости)
- [License and Copyright](#license-and-copyright)

## Описание

В качестве пакетного менеджера Docker образ использует [poetry](https://python-poetry.org/) - мощный инструмент для управления зависимостями. Благодаря заранее прописанным группам зависимостей в Docker образ устанавливаются только зависимости группы `dev`. При желании пользователь может установить зависимости группы `main`, но для отработки демонстрации хватит группа `dev`.

## Быстрый старт

1. Добавьте Docker образ в Images.

   ```docker
   docker build . -t start_and_format
   ```

2. Запустите Docker образ из терминала.

```docker
docker run -it -p 8888:8888 -v /path/to/home/directory:/app --rm start_and_format
```
- Прокладываем порт `-p`, чтобы Docker принимал и отдавал (сохранял) измененные файлы, создав volume `-v /ПУТЬ_К_ДИРЕКТОРИИ:/ПУТЬ_WORKDIR`. С помощью `--rm` мы завершаем и удаляем запущенный контейнер. 

## Используя линтер и форматтеры

1. Добавьте Docker образ в Images.

   ```docker
   docker build . -t start_and_format
   ```

2. Запустите Docker образ из терминала.

```docker
docker run -it -p 8888:8888 -v /path/to/home/directory:/app --rm start_and_format /bin/bash
```
- С помощью `/bin/bash` мы запускаем внутри образа терминал [bash](https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html).

3. Далее будет запущен терминал bash в Docker образе `root@CONTAINER_ID:/app#`.

- Чтобы посмотреть установленные зависимости, запустите команду:
```bash
poetry show
```

#### flake8
- Чтобы воспользоваться линтером flake8, запустите команду:
```bash
poetry run flake8
```
Команда отобразить строки, в которых обнаружены нарушения PEP правил. Некоторые ограничения самого flake8 прописаны в pyproject.toml и могут быть изменены/добавлены пользователем.

#### ruff and isort
- Чтобы воспользоваться форматером `ruff`, запустите команды:
```bash
poetry run ruff check . --fix
```
Команда проверит и исправит строки, в которых обнаружены нарушения PEP правил. Убрав `--fix` будет доступен режим "только отобразить" без исправления.

- Чтобы воспользоваться форматером `isort`, запустите команды:
```bash
poetry run isort . -sort
```
Команда проверит и исправит строки, в которых обнаружены нарушения очередности по PEP правилам. Если мы хотим только отобразить нарушения (без исправления), то нужно воспользоваться командой `poetry run isort . --check --diff`.
## Используемые зависимости

- Виртуальная среда `pyenv-win`: `3.1.1`
- Локальная виртуальная среда для poetry `python`: `3.10.5`
	_Заметка: Локальная виртуальная среда нужна при конфликтных ситуациях, связанных с несовместимыми версиями зависимостей при установке. Poetry создает виртуальную среду при запуске `install`, версия которой прописана в `pyproject.toml` .
- Глобальная виртуальная среда `python`:  `3.12.2`
- `poetry`: `1.8.2`
- Группа зависимостей в `pyproject.toml` **main** (tool.poetry.dependencies)
	- os-sys:`0.0.1`
- Группа зависимостей в `pyproject.toml` **dev** (tool.poetry.group.dev.dependencies)
	- flake8: `7.0.0`
	- isort: `5.13.2`
	- mccabe: `0.7.0`
	- numpy: `1.26.4`
	- pycodestyle: `2.11.1`
	- pyflakes: `3.2.0`
	- ruff: `0.3.7`

## License and Copyright

- start_and_format is licensed under [MIT](http://opensource.org/licenses/mit-license.php) _2024_