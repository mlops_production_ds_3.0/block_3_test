FROM python:3.12-slim

WORKDIR /app

COPY pyproject.toml main.py ./

RUN pip install poetry==1.6.1
RUN poetry install --only dev

COPY . .

CMD [ "poetry", "run", "python", "main.py" ]